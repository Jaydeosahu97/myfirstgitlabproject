package com.jay.authdemowork.config;

import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
public class SpringSecurityConfig {
    private static final Logger log = LoggerFactory.getLogger(SpringSecurityConfig.class);
    //bean created for using in memory user details manager
    
    @Bean
   public InMemoryUserDetailsManager userDetailsManager() {
       log.info("Creating inMemoryUserDetailsManager");
       Function<String, String> encoder = (String string) -> passwordEncoder().encode(string);
        log.info("password encoded successfully");
        UserDetails userDetails = User.builder()
                                .passwordEncoder(encoder).username("jay").password("abcd1234").roles("USER")
                                .build();
        log.info("created inMemoryUserDetailsManager successfully");
        return new InMemoryUserDetailsManager(userDetails);
    }
    //below is the password encoder for the spring security
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
