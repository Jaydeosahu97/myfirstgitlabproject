package com.jay.authdemowork.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @GetMapping
    public String welcome() {
        return "Hello World!";
    }
    @GetMapping("demo-link")
    public String demo(){
        return "modified demo";
    }
}
