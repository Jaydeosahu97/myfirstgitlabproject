package com.jay.authdemowork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthDemoWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthDemoWorkApplication.class, args);
	}

}
